CREATE TABLE IF NOT EXISTS `book` (
    `uuid` varchar(36) NOT NULL PRIMARY KEY,
    `name` varchar(255) NOT NULL,
    `author` varchar(36) NOT NULL,
    `genre` varchar(36) NOT NULL,
    `createdTime` timestamp NOT NULL default current_timestamp,
    `updatedTime` timestamp NOT NULL default current_timestamp
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
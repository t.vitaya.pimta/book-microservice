package com.example.demo.handler;

import com.example.demo.dto.BookDto;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.utils.Response;
import com.example.demo.utils.ResponseObject;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request
    ) {
        return ResponseEntity.ok(new ResponseObject<>(9000, "Mandatory missing"));
    }

    @ExceptionHandler(EntityNotFoundException.class)
    protected Response<BookDto> handleEntityNotFoundException(
            EntityNotFoundException ex,
            WebRequest request
    ) {
        return new Response<>(ex.getErrCode(), ex.getMessage());
    }

    @ExceptionHandler(DataAccessException.class)
    protected Response<BookDto> handleDataAccessException(
            EntityNotFoundException ex,
            WebRequest request
    ) {
        return new Response<>(500, "Something went wrong with database!");
    }
}

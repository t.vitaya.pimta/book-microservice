package com.example.demo.dto;

import lombok.Getter;

import java.util.List;

@Getter
public class BooksByGenreDto {
    private String uuid;
    private String name;
    private List<BookDto> bookList;

    public BooksByGenreDto(String uuid, String name, List<BookDto> bookList) {
        this.uuid = uuid;
        this.name = name;
        this.bookList = bookList;
    }

    public BooksByGenreDto() {}
}

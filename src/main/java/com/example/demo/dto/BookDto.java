package com.example.demo.dto;

import com.example.demo.models.Author;
import com.example.demo.models.Book;
import com.example.demo.models.Genre;
import lombok.Getter;

import java.sql.Timestamp;

@Getter
public class BookDto {
    private String uuid;
    private String name;
    private Author author;
    private Genre genre;
    private Timestamp createdTime;
    private Timestamp updatedTime;

    public BookDto(String uuid, String name, Author author, Genre genre, Timestamp createdTime, Timestamp updatedTime) {
        this.uuid = uuid;
        this.name = name;
        this.author = author;
        this.genre = genre;
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
    }

    public BookDto() {}
}

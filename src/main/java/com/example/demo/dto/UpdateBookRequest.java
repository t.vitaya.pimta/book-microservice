package com.example.demo.dto;

import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
public class UpdateBookRequest {
    @NotBlank
    private String name;
    @NotBlank
    private String authorId;
    private String genreId;

    public UpdateBookRequest(String name, String authorId, String genreId) {
        this.name = name;
        this.authorId = authorId;
        this.genreId = genreId;
    }
}

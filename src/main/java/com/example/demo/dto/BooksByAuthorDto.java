package com.example.demo.dto;

import lombok.Getter;

import java.util.List;

@Getter
public class BooksByAuthorDto {
    private String uuid;
    private String name;
    private List<BookDto> bookList;

    public BooksByAuthorDto(String uuid, String name, List<BookDto> bookList) {
        this.uuid = uuid;
        this.name = name;
        this.bookList = bookList;
    }

    public BooksByAuthorDto() {}
}

package com.example.demo.dto;

import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
public class DeleteBookRequest {
    @NotBlank
    private String name;
    @NotBlank
    private String authorId;
    private String genreId;

}

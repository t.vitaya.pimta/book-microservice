package com.example.demo.dto;

import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
public class CreateBookRequest {
    @NotBlank
    private String name;
    @NotBlank
    private String authorId;
    @NotBlank
    private String genreId;

    public CreateBookRequest(String name, String authorId, String genreId) {
        this.name = name;
        this.authorId = authorId;
        this.genreId = genreId;
    }
}

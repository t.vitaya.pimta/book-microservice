package com.example.demo.controllers;

import com.example.demo.dto.*;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.services.BookService;
import com.example.demo.utils.Response;
import com.example.demo.utils.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class BookController {

    @Autowired
    BookService bookService;
    @GetMapping("/")
    public Response<List<BookDto>> getBooks() throws EntityNotFoundException {
        List<BookDto> books = this.bookService.getBooks();
        return new Response<>(books);
    }

    @GetMapping("/{bookId}")
    public Response<BookDto> getBook(@PathVariable String bookId) throws EntityNotFoundException {
        BookDto book = this.bookService.getBook(bookId);
        return new Response<>(book);
    }

    @PostMapping("/")
    public Response<BookDto> createBook(@Valid @RequestBody CreateBookRequest req) throws EntityNotFoundException {
        BookDto newBook = this.bookService.create(req.getName(), req.getAuthorId(), req.getGenreId());
        return new Response<>(newBook);
    }

    @PutMapping("/{bookId}")
    public Response<BookDto> updateBook(@PathVariable String bookId, @Valid @RequestBody UpdateBookRequest req) throws EntityNotFoundException {
        BookDto updatedBook = this.bookService.update(bookId, req.getName(), req.getAuthorId(), req.getGenreId());
        return new Response<>(updatedBook);
    }

    @DeleteMapping("/{bookId}")
    public Response<BookDto> deleteBook(@PathVariable String bookId, @Valid @RequestBody DeleteBookRequest req) throws EntityNotFoundException {
        this.bookService.delete(bookId, req.getAuthorId(), req.getGenreId());
        return new Response<>();
    }

    @GetMapping("/{authorId}/author")
    public Response<BooksByAuthorDto> getBooksByAuthor(@PathVariable String authorId) throws EntityNotFoundException {
        BooksByAuthorDto booksByAuthorDto = this.bookService.getBooksByAuthor(authorId);
        return new Response<>(booksByAuthorDto);
    }

    @GetMapping("/{genreId}/genre")
    public Response<BooksByGenreDto> getBooksByGenre(@PathVariable String genreId) throws EntityNotFoundException {
        BooksByGenreDto booksByGenreDto = this.bookService.getBooksByGenre(genreId);
        return new Response<>(booksByGenreDto);
    }
}

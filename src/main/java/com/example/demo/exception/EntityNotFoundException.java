package com.example.demo.exception;

import lombok.Getter;

@Getter
public class EntityNotFoundException extends Exception {
    private int errCode;
    public EntityNotFoundException(int errCode, String errMessage) {
        super(errMessage);
        this.errCode = errCode;
    }
}

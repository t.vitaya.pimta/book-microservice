package com.example.demo.repository;

import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.models.Genre;

public interface GenreRepository {

    Genre getById(String id) throws EntityNotFoundException;
}

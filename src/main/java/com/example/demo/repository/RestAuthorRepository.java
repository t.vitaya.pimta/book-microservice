package com.example.demo.repository;

import com.example.demo.utils.ResponseObject;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.models.Author;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestAuthorRepository implements AuthorRepository{

    @Value("${api.author.url}")
    private String authorBaseUrl;
    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public Author getById(String id) throws EntityNotFoundException {
        String url = authorBaseUrl + id;
        ResponseEntity<ResponseObject<Author>> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<ResponseObject<Author>>() {}
        );
        ResponseObject<Author> res = response.getBody();

        if(res.getStatus().getCode() == 2000) {
            throw new EntityNotFoundException(2000, "author not found");
        }

        Author author = res.getData();
        return author;
    }
}

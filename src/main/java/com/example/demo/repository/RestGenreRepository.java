package com.example.demo.repository;

import com.example.demo.utils.ResponseObject;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.models.Genre;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestGenreRepository implements GenreRepository{

    @Value("${api.genre.url}")
    private String genreBaseUrl;
    private RestTemplate restTemplate = new RestTemplate();
    @Override
    public Genre getById(String id) throws EntityNotFoundException {
        String url = genreBaseUrl + id;
        ResponseEntity<ResponseObject<Genre>> response = restTemplate.exchange(url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<ResponseObject<Genre>>() {});
        ResponseObject<Genre> res = response.getBody();

        if(res.getStatus().getCode() == 3000) {
            throw new EntityNotFoundException(3000, "genre not found");
        }

        Genre genre = res.getData();
        return genre;
    }
}

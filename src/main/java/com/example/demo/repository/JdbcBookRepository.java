package com.example.demo.repository;

import com.example.demo.mapper.BookMapper;
import com.example.demo.models.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Component
public class JdbcBookRepository implements BookRepository{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Book> getAll() throws DataAccessException {
        return jdbcTemplate.query(
                "select * from book",
                new BookMapper()
        );
    }

    @Override
    public Book getById(String id) throws DataAccessException {
        return jdbcTemplate.queryForObject(
                "select * from book where uuid = ?",
                new Object[]{id},
                new BookMapper()
        );
    }

    @Override
    public void create(Book book) throws DataAccessException {
        jdbcTemplate.update(
                "insert into book (uuid, name, author, genre, createdTime, updatedTime) values(?,?,?,?,?,?)",
                book.getUuid(), book.getName(), book.getAuthorId(), book.getGenreId(), book.getCreatedTime(), book.getUpdatedTime()
        );
    }

    @Override
    public void update(String id, Book book) throws DataAccessException {
        jdbcTemplate.update(
                "update book set name = ?, author = ?, genre = ?, updatedTime = ? where uuid = ?",
                book.getName(), book.getAuthorId(), book.getGenreId(), Timestamp.valueOf(LocalDateTime.now()), id
        );
    }

    @Override
    public void delete(String id) throws DataAccessException {
        jdbcTemplate.update(
                "delete from book where uuid = ?",
                id
        );
    }

    @Override
    public int countBookById(String id) throws DataAccessException {
        return jdbcTemplate.queryForObject(
                "select count(*) from book where uuid = ?",
                new Object[]{id},
                Integer.class
        );
    }

    @Override
    public List<Book> getByAuthorId(String id) throws DataAccessException {
        return jdbcTemplate.query(
                "select * from book where author = ?",
                new Object[]{id},
                new BookMapper()

        );
    }

    @Override
    public List<Book> getByGenreId(String id) throws DataAccessException {
        return jdbcTemplate.query(
                "select * from book where genre = ?",
                new Object[]{id},
                new BookMapper()
        );
    }
}

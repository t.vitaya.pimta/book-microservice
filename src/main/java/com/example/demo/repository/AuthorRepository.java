package com.example.demo.repository;

import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.models.Author;

public interface AuthorRepository {

    Author getById(String id) throws EntityNotFoundException;
}

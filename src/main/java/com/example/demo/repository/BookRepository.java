package com.example.demo.repository;

import com.example.demo.models.Book;

import java.util.List;

public interface BookRepository {
    public List<Book> getAll();
    public Book getById(String id);
    public void create(Book book);
    public void update(String id, Book book);
    public void delete(String id);

    public int countBookById(String id);

    List<Book> getByAuthorId(String id);
    List<Book> getByGenreId(String id);
}



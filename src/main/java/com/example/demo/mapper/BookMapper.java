package com.example.demo.mapper;

import com.example.demo.models.Book;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookMapper implements RowMapper<Book> {
    @Override
    public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Book(
                rs.getString("uuid"),
                rs.getString("name"),
                rs.getString("author"),
                rs.getString("genre"),
                rs.getTimestamp("createdTime"),
                rs.getTimestamp("updatedTime")
        );
    }
}

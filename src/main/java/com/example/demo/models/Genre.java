package com.example.demo.models;

import lombok.Getter;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Getter
public class Genre {
    private String uuid;
    private String name;
    private Timestamp createdTime;
    private Timestamp updatedTime;

    public Genre(String uuid, String name, Timestamp createdTime, Timestamp updatedTime) {
        this(uuid, name);
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
    }

    public Genre(String uuid, String name) {
        this.uuid = uuid;
        this.name = name;
        this.createdTime = Timestamp.valueOf(LocalDateTime.now());
        this.updatedTime = Timestamp.valueOf(LocalDateTime.now());
    }

    public Genre() {}
}

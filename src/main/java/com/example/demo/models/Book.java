package com.example.demo.models;

import lombok.Getter;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Getter
public class Book {
    private String uuid;
    private String name;
    private String authorId;
    private String genreId;
    private Timestamp createdTime;
    private Timestamp updatedTime;

    public Book(String name, String authorId, String genreId) {
        this.name = name;
        this.authorId = authorId;
        this.genreId = genreId;
        this.createdTime = Timestamp.valueOf(LocalDateTime.now());
        this.updatedTime = Timestamp.valueOf(LocalDateTime.now());
    }

    public Book(String uuid, String name, String authorId, String genreId) {
        this(name, authorId, genreId);
        this.uuid = uuid;
    }

    public Book(String uuid, String name, String authorId, String genreId, Timestamp createdTime, Timestamp updatedTime) {
        this(uuid, name, authorId, genreId);
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
    }

    public Book() {}

    public void update(String name, String authorId, String genreId) {
        this.name = name;
        this.authorId = authorId;

        if(genreId != null) {
            this.genreId = genreId;
        }
    }

}

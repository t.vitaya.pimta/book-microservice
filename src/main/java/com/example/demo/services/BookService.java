package com.example.demo.services;

import com.example.demo.dto.BookDto;
import com.example.demo.dto.BooksByAuthorDto;
import com.example.demo.dto.BooksByGenreDto;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.models.Author;
import com.example.demo.models.Book;
import com.example.demo.models.Genre;
import com.example.demo.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class BookService {

    @Autowired
    JdbcBookRepository bookRepository;

    @Autowired
    RestAuthorRepository authorRepository;

    @Autowired
    RestGenreRepository genreRepository;

    public List<BookDto> getBooks() throws DataAccessException, EntityNotFoundException {
        List<Book> books = bookRepository.getAll();
        List<BookDto> booksDto = new ArrayList<>();

        for(Book book: books) {
            Author author = this.authorRepository.getById(book.getAuthorId());
            Genre genre = this.genreRepository.getById(book.getGenreId());

            booksDto.add(this.bookToBookDto(book, author, genre));
        }

        return booksDto;
    }

    public BookDto getBook(String bookId) throws EntityNotFoundException {
        try {
            Book book = bookRepository.getById(bookId);
            Author author = authorRepository.getById(book.getAuthorId());
            Genre genre = genreRepository.getById(book.getGenreId());

            return this.bookToBookDto(book, author, genre);
        }
        catch (DataAccessException e) {
            throw new EntityNotFoundException(4000, "book not found");
        }
    }

    public BookDto create(String name, String authorId, String genreId) throws DataAccessException, EntityNotFoundException {
        Author author = this.authorRepository.getById(authorId);
        Genre genre = this.genreRepository.getById(genreId);

        Book newBook = new Book(UUID.randomUUID().toString(), name, authorId, genreId, Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        this.bookRepository.create(newBook);

        return this.bookToBookDto(newBook, author, genre);
    }

    public BookDto update(String bookId, String name, String authorId, String genreId) throws EntityNotFoundException, DataAccessException {
        try {
            Author author = this.authorRepository.getById(authorId);
            Genre genre = this.genreRepository.getById(genreId);
            Book book = bookRepository.getById(bookId);

            book.update(name, authorId, genreId);
            this.bookRepository.update(bookId, book);

            return this.bookToBookDto(book, author, genre);
        }
        catch (DataAccessException e) {
            throw new EntityNotFoundException(4000, "book not found");
        }
    }

    public void delete(String bookId, String authorId, String genreId) throws DataAccessException, EntityNotFoundException {
        try {
            this.bookRepository.getById(bookId);
            this.authorRepository.getById(authorId);
            this.genreRepository.getById(genreId);

            this.bookRepository.delete(bookId);
        }
        catch (DataAccessException e) {
            throw new EntityNotFoundException(4000, "book not found");
        }
    }

    public BooksByAuthorDto getBooksByAuthor(String authorId) throws EntityNotFoundException {
        try {
            Author author = this.authorRepository.getById(authorId);
            List<Book> books = this.bookRepository.getByAuthorId(authorId);
            List<BookDto> booksDto = new ArrayList<>();

            for(Book book: books) {
                Genre genre = this.genreRepository.getById(book.getGenreId());

                booksDto.add(this.bookToBookDto(book, author, genre));
            }

            BooksByAuthorDto booksByAuthorDto = new BooksByAuthorDto(author.getUuid(), author.getName(), booksDto);

            return booksByAuthorDto;
        }
        catch (DataAccessException e) {
            throw new EntityNotFoundException(4000, "Author not found");
        }
    }

    public BooksByGenreDto getBooksByGenre(String genreId) throws EntityNotFoundException {
        try {
            Genre genre = this.genreRepository.getById(genreId);
            List<Book> books = this.bookRepository.getByGenreId(genreId);
            List<BookDto> booksDto = new ArrayList<>();

            for(Book book: books) {
                Author author = this.authorRepository.getById(book.getAuthorId());

                booksDto.add(this.bookToBookDto(book, author, genre));
            }

            BooksByGenreDto booksByGenreDto = new BooksByGenreDto(genre.getUuid(), genre.getName(), booksDto);

            return booksByGenreDto;
        }
        catch (DataAccessException e) {
            throw new EntityNotFoundException(4000, "Genre not found");
        }
    }
    private BookDto bookToBookDto(Book book, Author author, Genre genre) {

        return new BookDto(
                book.getUuid(),
                book.getName(),
                author,
                genre,
                book.getCreatedTime(),
                book.getUpdatedTime()
        );
    }
}

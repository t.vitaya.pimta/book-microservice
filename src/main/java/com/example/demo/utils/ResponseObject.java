package com.example.demo.utils;

import lombok.Getter;

@Getter
public class ResponseObject<T> {
    private Status status;
    private T data;

    public ResponseObject(int code, String description, T data) {
        this(code, description);
        this.data = data;
    }

    public ResponseObject(int code, String description) {
        this.status = new Status(code, description);
    }

    public ResponseObject() {}
}

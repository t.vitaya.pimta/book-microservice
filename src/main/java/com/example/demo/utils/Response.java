package com.example.demo.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Response<T> extends ResponseEntity<ResponseObject<T>> {
    public Response(int code, String description, T obj) {
        super(new ResponseObject<T>(code, description, (T) obj), HttpStatus.OK);
    }

    public Response(int code, String description) {
        super(new ResponseObject<T>(code, description, (T) null), HttpStatus.OK);
    }

    public Response(T obj) {
        super(new ResponseObject<T>(200, "OK",(T) obj), HttpStatus.OK);
    }

    public Response() {
        super(new ResponseObject<T>(200, "OK", (T) null), HttpStatus.OK);
    }
}

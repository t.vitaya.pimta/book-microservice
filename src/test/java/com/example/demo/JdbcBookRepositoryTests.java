package com.example.demo;

import com.example.demo.mapper.BookMapper;
import com.example.demo.models.Book;
import com.example.demo.repository.JdbcBookRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class JdbcBookRepositoryTests {
    @Mock
    JdbcTemplate jdbcTemplate;

    @InjectMocks
    JdbcBookRepository bookRepository;

    @Test
    public void getAllTest() {
        Book book1 = new Book("valid_bookId_1", "mvc_book_test_1", "valid_authorId_1", "valid_genreId_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        Book book2 = new Book("valid_bookId_2", "mvc_book_test_2", "valid_authorId_2", "valid_genreId_2", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        List<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);

        doReturn(books).when(jdbcTemplate).query(
                eq("select * from book"), any(BookMapper.class)
        );

        List<Book> dbBooks = bookRepository.getAll();

        Assertions.assertEquals(2, dbBooks.size());

        Assertions.assertEquals("valid_bookId_1", dbBooks.get(0).getUuid());
        Assertions.assertEquals("mvc_book_test_1", dbBooks.get(0).getName());
        Assertions.assertEquals("valid_authorId_1", dbBooks.get(0).getAuthorId());
        Assertions.assertEquals("valid_genreId_1", dbBooks.get(0).getGenreId());

        Assertions.assertEquals("valid_bookId_2", dbBooks.get(1).getUuid());
        Assertions.assertEquals("mvc_book_test_2", dbBooks.get(1).getName());
        Assertions.assertEquals("valid_authorId_2", dbBooks.get(1).getAuthorId());
        Assertions.assertEquals("valid_genreId_2", dbBooks.get(1).getGenreId());
    }

    @Test
    public void getByIdTest() {
        Book book1 = new Book("valid_bookId_1", "mvc_book_test_1", "valid_authorId_1", "valid_genreId_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));

        doReturn(book1)
                .when(jdbcTemplate)
                .queryForObject(
                        eq("select * from book where uuid = ?"),
                        any(Object[].class),
                        any(BookMapper.class)
                );

        Book dbBook = bookRepository.getById("valid_bookId_1");

        Assertions.assertEquals("valid_bookId_1", dbBook.getUuid());
        Assertions.assertEquals("mvc_book_test_1", dbBook.getName());
        Assertions.assertEquals("valid_authorId_1", dbBook.getAuthorId());
        Assertions.assertEquals("valid_genreId_1", dbBook.getGenreId());
    }

    @Test
    public void getByIdTestFail() {
        doThrow(new DataAccessException("") {}).when(jdbcTemplate).queryForObject(
                eq("select * from book where uuid = ?"),
                any(Object[].class),
                any(BookMapper.class)
        );

        Assertions.assertThrows(DataAccessException.class, () -> bookRepository.getById("invalid_bookId_1"));
    }

    @Test
    public void createTest() {
        doReturn(1).when(jdbcTemplate).update(
                eq("insert into book (uuid, name, author, genre, createdTime, updatedTime) values(?,?,?,?,?,?)"),
                eq("valid_bookId_1"), eq("changed_mvc_book_test_1"), eq("valid_authorId_1"), eq("valid_genreId_1"), any(Timestamp.class), any(Timestamp.class)
        );

        Book book = new Book("valid_bookId_1", "changed_mvc_book_test_1", "valid_authorId_1", "valid_genreId_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));

        bookRepository.create(book);
    }

    @Test
    public void updateTest() {
        doReturn(1).when(jdbcTemplate).update(
                eq("update book set name = ?, author = ?, genre = ?, updatedTime = ? where uuid = ?"),
                eq("changed_mvc_book_test_1"), eq("changed_valid_authorId_1"), eq("changed_valid_genreId_1"), any(Timestamp.class), eq("valid_bookId_1")
        );

        Book book = new Book("valid_bookId_1", "changed_mvc_book_test_1", "changed_valid_authorId_1", "changed_valid_genreId_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));

        bookRepository.update("valid_bookId_1", book);
    }

    @Test
    public void updateTestFail() {
        doThrow(new DataAccessException("") {}).when(jdbcTemplate).update(
                eq("update book set name = ?, author = ?, genre = ?, updatedTime = ? where uuid = ?"),
                eq("changed_mvc_book_test_1"), eq("changed_valid_authorId_1"), eq("changed_valid_genreId_1"), any(Timestamp.class), eq("invalid_bookId_1")
        );

        Book book = new Book("valid_bookId_1", "changed_mvc_book_test_1", "changed_valid_authorId_1", "changed_valid_genreId_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));

        Assertions.assertThrows(DataAccessException.class, () -> bookRepository.update("invalid_bookId_1", book));
    }

    @Test
    public void deleteTest() {
        doReturn(1).when(jdbcTemplate).update(
                eq("delete from book where uuid = ?"),
                eq("valid_bookId_1")
        );

        bookRepository.delete("valid_bookId_1");
    }

    @Test
    public void deleteTestFail() {
        doThrow(new DataAccessException("") {}).when(jdbcTemplate).update(
                eq("delete from book where uuid = ?"),
                eq("invalid_bookId_1")
        );

        Assertions.assertThrows(DataAccessException.class, () -> bookRepository.delete("invalid_bookId_1"));
    }

    @Test
    public void getBooksByAuthor() {
        Book book1 = new Book("valid_bookId_1", "mvc_book_test_1", "valid_authorId_1", "valid_genreId_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        Book book2 = new Book("valid_bookId_2", "mvc_book_test_2", "valid_authorId_1", "valid_genreId_2", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        List<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);

        doReturn(books).when(jdbcTemplate).query(
                eq("select * from book where author = ?"),
                any(Object[].class),
                any(BookMapper.class)
        );

        List<Book> dbBooks = bookRepository.getByAuthorId("valid_authorId_1");

        Assertions.assertEquals("valid_bookId_1", dbBooks.get(0).getUuid());
        Assertions.assertEquals("mvc_book_test_1", dbBooks.get(0).getName());
        Assertions.assertEquals("valid_authorId_1", dbBooks.get(0).getAuthorId());
        Assertions.assertEquals("valid_genreId_1", dbBooks.get(0).getGenreId());

        Assertions.assertEquals("valid_bookId_2", dbBooks.get(1).getUuid());
        Assertions.assertEquals("mvc_book_test_2", dbBooks.get(1).getName());
        Assertions.assertEquals("valid_authorId_1", dbBooks.get(1).getAuthorId());
        Assertions.assertEquals("valid_genreId_2", dbBooks.get(1).getGenreId());
    }

    @Test
    public void getBooksByAuthorFail() {
        doThrow(new DataAccessException("") {}).when(jdbcTemplate).query(
                eq("select * from book where author = ?"),
                any(Object[].class),
                any(BookMapper.class)
        );

        Assertions.assertThrows(DataAccessException.class, () -> bookRepository.getByAuthorId("invalid_genreId_1"));
    }

    @Test
    public void getBooksByGenre() {
        Book book1 = new Book("valid_bookId_1", "mvc_book_test_1", "valid_authorId_1", "valid_genreId_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        Book book2 = new Book("valid_bookId_2", "mvc_book_test_2", "valid_authorId_2", "valid_genreId_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        List<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);

        doReturn(books).when(jdbcTemplate).query(
                eq("select * from book where genre = ?"),
                any(Object[].class),
                any(BookMapper.class)
        );

        List<Book> dbBooks = bookRepository.getByGenreId("valid_genreId_1");

        Assertions.assertEquals("valid_bookId_1", dbBooks.get(0).getUuid());
        Assertions.assertEquals("mvc_book_test_1", dbBooks.get(0).getName());
        Assertions.assertEquals("valid_authorId_1", dbBooks.get(0).getAuthorId());
        Assertions.assertEquals("valid_genreId_1", dbBooks.get(0).getGenreId());

        Assertions.assertEquals("valid_bookId_2", dbBooks.get(1).getUuid());
        Assertions.assertEquals("mvc_book_test_2", dbBooks.get(1).getName());
        Assertions.assertEquals("valid_authorId_2", dbBooks.get(1).getAuthorId());
        Assertions.assertEquals("valid_genreId_1", dbBooks.get(1).getGenreId());
    }

    @Test
    public void getBooksByGenreFail() {
        doThrow(new DataAccessException("") {}).when(jdbcTemplate).query(
                eq("select * from book where genre = ?"),
                any(Object[].class),
                any(BookMapper.class)
        );

        Assertions.assertThrows(DataAccessException.class, () -> bookRepository.getByGenreId("invalid_genreId_1"));
    }
}

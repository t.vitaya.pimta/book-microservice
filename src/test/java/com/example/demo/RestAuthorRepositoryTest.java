package com.example.demo;

import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.models.Author;
import com.example.demo.repository.RestAuthorRepository;
import com.example.demo.utils.Response;
import com.example.demo.utils.ResponseObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
public class RestAuthorRepositoryTest {
    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private RestAuthorRepository authorRepository;

    @Test
    public void getById() throws EntityNotFoundException {
        ReflectionTestUtils.setField(authorRepository, "authorBaseUrl", "/");
        Author author = new Author("valid_authorId_1", "author_test_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        String url = "/valid_authorId_1";

        doReturn(new Response<Author>(author)).when(restTemplate).exchange(
                eq(url),
                eq(HttpMethod.GET),
                Matchers.<HttpEntity<ResponseObject<Author>>>any(),
                Matchers.<ParameterizedTypeReference<ResponseObject<Author>>>any()
        );

        Author getAuthor = authorRepository.getById("valid_authorId_1");

        Assertions.assertEquals("valid_authorId_1", getAuthor.getUuid());
        Assertions.assertEquals("author_test_1", getAuthor.getName());
    }

    @Test
    public void getByIdFail() {
        ReflectionTestUtils.setField(authorRepository, "authorBaseUrl", "/");
        String url = "/invalid_authorId_1";

        doReturn(new Response<>(2000, "author not found")).when(restTemplate).exchange(
                eq(url),
                eq(HttpMethod.GET),
                Matchers.<HttpEntity<ResponseObject<Author>>>any(),
                Matchers.<ParameterizedTypeReference<ResponseObject<Author>>>any()
        );

        Assertions.assertThrows(EntityNotFoundException.class, () -> authorRepository.getById("invalid_authorId_1"));
    }
}

package com.example.demo;

import com.example.demo.dto.BookDto;
import com.example.demo.dto.BooksByAuthorDto;
import com.example.demo.dto.BooksByGenreDto;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.models.Author;
import com.example.demo.models.Book;
import com.example.demo.models.Genre;
import com.example.demo.repository.*;
import com.example.demo.services.BookService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataAccessException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BookServiceTests {
    @Mock
    private JdbcBookRepository bookRepository;
    @Mock
    private RestAuthorRepository authorRepository;
    @Mock
    private RestGenreRepository genreRepository;
    @InjectMocks
    private BookService bookService;

    @Test
    public void shouldReturnBooks() throws EntityNotFoundException {
        Assertions.assertEquals(bookService.getBooks(), List.of());
    }

    @Test
    public void shouldReturnBookWhenGivenId() throws EntityNotFoundException {
        doReturn(new Book("valid_book_id", "book_testing", "valid_author_id", "valid_genre_id"))
                .when(bookRepository)
                .getById("valid_book_id");
        doReturn(new Author("valid_author_id", "author_testing"))
                .when(authorRepository)
                .getById("valid_author_id");
        doReturn(new Genre("valid_genre_id", "genre_testing"))
                .when(genreRepository)
                .getById("valid_genre_id");

        BookDto bookDto = bookService.getBook("valid_book_id");

        Assertions.assertEquals(bookDto.getUuid(), "valid_book_id");
        Assertions.assertEquals(bookDto.getName(), "book_testing");

        Assertions.assertEquals(bookDto.getAuthor().getUuid(), "valid_author_id");
        Assertions.assertEquals(bookDto.getAuthor().getName(), "author_testing");

        Assertions.assertEquals(bookDto.getGenre().getUuid(), "valid_genre_id");
        Assertions.assertEquals(bookDto.getGenre().getName(), "genre_testing");
    }

    @Test
    public void shouldFailWhenGivenInvalidId() {
        doThrow(new DataAccessException("") {})
                .when(bookRepository)
                .getById("invalid_book_id");

        Assertions.assertThrows(EntityNotFoundException.class, () -> bookService.getBook("invalid_book_id"));
    }

    @Test
    public void shouldCreateBook() throws EntityNotFoundException {
        doReturn(new Author("valid_author_id", "author_testing"))
                .when(authorRepository)
                .getById("valid_author_id");
        doReturn(new Genre("valid_genre_id", "genre_testing"))
                .when(genreRepository)
                .getById("valid_genre_id");

        BookDto bookDto = bookService.create("book_testing", "valid_author_id", "valid_genre_id");

        Assertions.assertNotNull(bookDto.getUuid());
        Assertions.assertEquals(bookDto.getName(), "book_testing");

        Assertions.assertEquals(bookDto.getAuthor().getUuid(), "valid_author_id");
        Assertions.assertEquals(bookDto.getAuthor().getName(), "author_testing");

        Assertions.assertEquals(bookDto.getGenre().getUuid(), "valid_genre_id");
        Assertions.assertEquals(bookDto.getGenre().getName(), "genre_testing");
    }

    private static Stream<Arguments> provideArgumentsForFailToCreateBookWhenGivenInvalidId() {
        return Stream.of(
                Arguments.of("book_testing", "invalid_author_id", "valid_genre_id"),
                Arguments.of("book_testing", "valid_author_id", "invalid_genre_id")
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForFailToCreateBookWhenGivenInvalidId")
    public void shouldFailToCreateBookWhenGivenInvalidId(String name, String authorId, String genreId) throws EntityNotFoundException {
        lenient().doThrow(EntityNotFoundException.class)
                .when(authorRepository)
                .getById("invalid_author_id");
        lenient().doThrow(EntityNotFoundException.class)
                .when(genreRepository)
                .getById("invalid_genre_id");

        Assertions.assertThrows(EntityNotFoundException.class, () -> bookService.create(name, authorId, genreId));
    }

    @Test
    public void shouldUpdateBook() throws EntityNotFoundException {
        doReturn(new Book("valid_book_id", "book_testing", "valid_author_id", "valid_genre_id"))
                .when(bookRepository)
                .getById("valid_book_id");
        doReturn(new Author("valid_changed_author_id", "changed_author_testing"))
                .when(authorRepository)
                .getById("valid_changed_author_id");
        doReturn(new Genre("valid_changed_genre_id", "changed_genre_testing"))
                .when(genreRepository)
                .getById("valid_changed_genre_id");

        BookDto bookDto = bookService.update("valid_book_id", "changed_book_testing", "valid_changed_author_id", "valid_changed_genre_id");

        Assertions.assertNotNull(bookDto.getUuid(), "valid_book_id");
        Assertions.assertEquals(bookDto.getName(), "changed_book_testing");

        Assertions.assertEquals(bookDto.getAuthor().getUuid(), "valid_changed_author_id");
        Assertions.assertEquals(bookDto.getAuthor().getName(), "changed_author_testing");

        Assertions.assertEquals(bookDto.getGenre().getUuid(), "valid_changed_genre_id");
        Assertions.assertEquals(bookDto.getGenre().getName(), "changed_genre_testing");
    }

    private static Stream<Arguments> provideArgumentsForFailToUpdateBookWhenGivenInvalidId() {
        return Stream.of(
                Arguments.of("invalid_book_id", "changed_name", "valid_author_id", "valid_genre_id"),
                Arguments.of("valid_book_id", "changed_name", "invalid_author_id", "valid_genre_id"),
                Arguments.of("valid_book_id", "changed_name", "valid_author_id", "invalid_genre_id")
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForFailToUpdateBookWhenGivenInvalidId")
    public void shouldFailToUpdateWhenGivenInvalidId(String bookId, String name, String authorId, String genreId) throws EntityNotFoundException {
        lenient().doThrow(new DataAccessException("") {})
                .when(bookRepository)
                .getById("invalid_book_id");
        lenient().doThrow(EntityNotFoundException.class)
                .when(authorRepository)
                .getById("invalid_author_id");
        lenient().doThrow(EntityNotFoundException.class)
                .when(genreRepository)
                .getById("invalid_genre_id");

        Assertions.assertThrows(
                EntityNotFoundException.class,
                () -> bookService.update(bookId, name, authorId, genreId)
        );
    }

    @Test
    public void shouldDeleteBook() throws EntityNotFoundException {
        doReturn(new Book("valid_book_id", "book_testing", "valid_author_id", "valid_genre_id"))
                .when(bookRepository)
                .getById("valid_book_id");
        doReturn(new Author("valid_author_id", "author_testing"))
                .when(authorRepository)
                .getById("valid_author_id");
        doReturn(new Genre("valid_genre_id", "genre_testing"))
                .when(genreRepository)
                .getById("valid_genre_id");

        bookService.delete("valid_book_id", "valid_author_id", "valid_genre_id");
    }

    private static Stream<Arguments> provideArgumentsForFailToDeleteBookWhenGivenInvalidId() {
        return Stream.of(
                Arguments.of("invalid_book_id", "valid_author_id", "valid_genre_id"),
                Arguments.of("valid_book_id", "invalid_author_id", "valid_genre_id"),
                Arguments.of("valid_book_id", "valid_author_id", "invalid_genre_id")
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForFailToDeleteBookWhenGivenInvalidId")
    public void shouldFailToDeleteWhenGivenInvalidBookId(String bookId, String authorId, String genreId) throws EntityNotFoundException {
        lenient().doThrow(new DataAccessException("") {})
                .when(bookRepository)
                .getById("invalid_book_id");
        lenient().doThrow(EntityNotFoundException.class)
                .when(authorRepository)
                .getById("invalid_author_id");
        lenient().doThrow(EntityNotFoundException.class)
                .when(genreRepository)
                .getById("invalid_genre_id");

        Assertions.assertThrows(
                EntityNotFoundException.class,
                () -> bookService.delete(bookId, authorId, genreId)
        );
    }

    @Test
    public void shouldReturnBooksByAuthorWhenGivenAuthorId() throws EntityNotFoundException {
        doReturn(new Author("valid_author_id", "author_testing"))
                .when(authorRepository)
                .getById("valid_author_id");
        doReturn(new ArrayList<BookDto>())
                .when(bookRepository)
                .getByAuthorId("valid_author_id");

        BooksByAuthorDto booksByAuthorDto = bookService.getBooksByAuthor("valid_author_id");

        Assertions.assertEquals(booksByAuthorDto.getUuid(), "valid_author_id");
        Assertions.assertEquals(booksByAuthorDto.getName(), "author_testing");
        Assertions.assertEquals(booksByAuthorDto.getBookList(), List.of());
    }

    @Test
    public void shouldFailToGetBooksByAuthorWhenGivenInvalidId() throws EntityNotFoundException {
        doThrow(EntityNotFoundException.class)
                .when(authorRepository)
                .getById("invalid_author_id");

        Assertions.assertThrows(EntityNotFoundException.class, () -> bookService.getBooksByAuthor("invalid_author_id"));
    }

    @Test
    public void shouldReturnBooksByGenreWhenGivenAuthorId() throws EntityNotFoundException {
        doReturn(new Genre("valid_genre_id", "genre_testing"))
                .when(genreRepository)
                .getById("valid_genre_id");
        doReturn(new ArrayList<BookDto>())
                .when(bookRepository)
                .getByGenreId("valid_genre_id");

        BooksByGenreDto booksByGenreDto = bookService.getBooksByGenre("valid_genre_id");
        Assertions.assertEquals(booksByGenreDto.getUuid(), "valid_genre_id");
        Assertions.assertEquals(booksByGenreDto.getName(), "genre_testing");
        Assertions.assertEquals(booksByGenreDto.getBookList(), List.of());
    }

    @Test
    public void shouldFailToGetBooksByGenreWhenGivenInvalidId() throws EntityNotFoundException {
        doThrow(EntityNotFoundException.class)
                .when(genreRepository)
                .getById("invalid_genre_id");

        Assertions.assertThrows(EntityNotFoundException.class, () -> bookService.getBooksByGenre("invalid_genre_id"));
    }
}

package com.example.demo;

import com.example.demo.dto.CreateBookRequest;
import com.example.demo.dto.UpdateBookRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;
import java.util.stream.Stream;

public class BookValidationTests {

    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Test
    public void shouldValidateSuccessToCreateBook() {
        CreateBookRequest req = new CreateBookRequest("not blank", "not blank", "not blank");
        Set<ConstraintViolation<CreateBookRequest>> violations = validator.validate(req);

        Assertions.assertTrue(violations.isEmpty());
    }

    private static Stream<Arguments> provideArgumentsForFailToCreateBook() {
        return Stream.of(
                Arguments.of("", "not blank", "not blank"),
                Arguments.of("not blank", "", "not blank"),
                Arguments.of("not blank", "not blank", ""),
                Arguments.of("", "", "not blank"),
                Arguments.of("", "not blank", ""),
                Arguments.of("not blank", "", ""),
                Arguments.of("", "", ""),
                Arguments.of(null, "not blank", "not blank"),
                Arguments.of("not blank", null, "not blank"),
                Arguments.of("not blank", "not blank", null),
                Arguments.of(null, null, "not blank"),
                Arguments.of(null, "not blank", null),
                Arguments.of("not blank", null, null),
                Arguments.of(null, null, null)
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForFailToCreateBook")
    public void shouldValidateFailToCreateBook(String name, String authorId, String genreId) {
        CreateBookRequest req = new CreateBookRequest(name, authorId, genreId);
        Set<ConstraintViolation<CreateBookRequest>> violations = validator.validate(req);

        Assertions.assertFalse(violations.isEmpty());
    }

    private static Stream<Arguments> provideArgumentsForFailToUpdateBook() {
        return Stream.of(
                Arguments.of("", "not blank", "not blank"),
                Arguments.of("not blank", "", "not blank"),
                Arguments.of("", "", "not blank"),
                Arguments.of("", "not blank", ""),
                Arguments.of("not blank", "", ""),
                Arguments.of("", "", ""),
                Arguments.of(null, "not blank", "not blank"),
                Arguments.of("not blank", null, "not blank"),
                Arguments.of(null, null, "not blank"),
                Arguments.of(null, "not blank", null),
                Arguments.of("not blank", null, null),
                Arguments.of(null, null, null)
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForFailToUpdateBook")
    public void shouldValidateFailToUpdateBook(String name, String authorId, String genreId) {
        UpdateBookRequest req = new UpdateBookRequest(name, authorId, genreId);
        Set<ConstraintViolation<UpdateBookRequest>> violations = validator.validate(req);

        Assertions.assertFalse(violations.isEmpty());
    }

    private static Stream<Arguments> provideArgumentsForSuccessToUpdateBook() {
        return Stream.of(
                Arguments.of("not blank", "not blank", ""),
                Arguments.of("not blank", "not blank", null)
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForSuccessToUpdateBook")
    public void shouldValidateSuccessToUpdateBook(String name, String authorId, String genreId) {
        UpdateBookRequest req = new UpdateBookRequest(name, authorId, genreId);
        Set<ConstraintViolation<UpdateBookRequest>> violations = validator.validate(req);

        Assertions.assertTrue(violations.isEmpty());
    }

    private static Stream<Arguments> provideArgumentsForFailToDeleteBook() {
        return Stream.of(
                Arguments.of("", "not blank", "not blank"),
                Arguments.of("not blank", "", "not blank"),
                Arguments.of("", "", "not blank"),
                Arguments.of("", "not blank", ""),
                Arguments.of("not blank", "", ""),
                Arguments.of("", "", ""),
                Arguments.of(null, "not blank", "not blank"),
                Arguments.of("not blank", null, "not blank"),
                Arguments.of(null, null, "not blank"),
                Arguments.of(null, "not blank", null),
                Arguments.of("not blank", null, null),
                Arguments.of(null, null, null)
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForFailToDeleteBook")
    public void shouldValidateFailToDeleteBook(String name, String authorId, String genreId) {
        UpdateBookRequest req = new UpdateBookRequest(name, authorId, genreId);
        Set<ConstraintViolation<UpdateBookRequest>> violations = validator.validate(req);

        Assertions.assertFalse(violations.isEmpty());
    }

    private static Stream<Arguments> provideArgumentsForSuccessToDeleteBook() {
        return Stream.of(
                Arguments.of("not blank", "not blank", ""),
                Arguments.of("not blank", "not blank", null)
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForSuccessToDeleteBook")
    public void shouldValidateSuccessToDeleteBook(String name, String authorId, String genreId) {
        UpdateBookRequest req = new UpdateBookRequest(name, authorId, genreId);
        Set<ConstraintViolation<UpdateBookRequest>> violations = validator.validate(req);

        Assertions.assertTrue(violations.isEmpty());
    }
}

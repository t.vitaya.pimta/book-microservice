package com.example.demo;

import com.example.demo.dto.BookDto;
import com.example.demo.dto.BooksByAuthorDto;
import com.example.demo.dto.BooksByGenreDto;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.models.Author;
import com.example.demo.models.Genre;
import com.example.demo.services.BookService;
import com.example.demo.utils.ResponseObject;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class BookMvcTests {
    @Autowired
    private MockMvc mvc;

    @MockBean
    BookService bookService;

    private List<BookDto> mockGetBooks() {
        Author author1 = new Author("valid_authorId_1", "mvc_author_test_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        Author author2 = new Author("valid_authorId_2", "mvc_author_test_2", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));

        Genre genre1 = new Genre("valid_genreId_1", "mvc_genre_test_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        Genre genre2 = new Genre("valid_genreId_2", "mvc_genre_test_2", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));

        BookDto book1 = new BookDto("valid_bookId_1", "mvc_book_test_1", author1, genre1, Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        BookDto book2 = new BookDto("valid_bookId_2", "mvc_book_test_2", author2, genre2 , Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));

        List<BookDto> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);

        return books;
    }

    @Test
    public void getAllBooksWithStatusCode200() throws Exception {
        doReturn(mockGetBooks()).when(bookService).getBooks();

        MvcResult mvcResult = this.mvc.perform(get("/")
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andReturn();
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        ResponseObject<List<BookDto>> response = mapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<ResponseObject<List<BookDto>>>() {});
        List<BookDto> books = response.getData();

        Assertions.assertEquals(2, books.size());

        Assertions.assertEquals("valid_bookId_1", books.get(0).getUuid());
        Assertions.assertEquals("mvc_book_test_1", books.get(0).getName());
        Assertions.assertEquals("valid_authorId_1", books.get(0).getAuthor().getUuid());
        Assertions.assertEquals("mvc_author_test_1", books.get(0).getAuthor().getName());
        Assertions.assertEquals("valid_genreId_1", books.get(0).getGenre().getUuid());
        Assertions.assertEquals("mvc_genre_test_1", books.get(0).getGenre().getName());

        Assertions.assertEquals("valid_bookId_2", books.get(1).getUuid());
        Assertions.assertEquals("mvc_book_test_2", books.get(1).getName());
        Assertions.assertEquals("valid_authorId_2", books.get(1).getAuthor().getUuid());
        Assertions.assertEquals("mvc_author_test_2", books.get(1).getAuthor().getName());
        Assertions.assertEquals("valid_genreId_2", books.get(1).getGenre().getUuid());
        Assertions.assertEquals("mvc_genre_test_2", books.get(1).getGenre().getName());
    }

    @Test
    public void getBookByIdWithStatusCode200() throws Exception {
        Author author1 = new Author("valid_authorId_1", "mvc_author_test_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        Genre genre1 = new Genre("valid_genreId_1", "mvc_genre_test_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));

        doReturn(new BookDto("valid_bookId_1", "mvc_book_test_1", author1, genre1, Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now())))
                .when(bookService)
                .getBook("valid_id");

        MvcResult mvcResult = mvc.perform(
                get("/valid_id")
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andReturn();
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        ResponseObject<BookDto> response = mapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<ResponseObject<BookDto>>() {});
        BookDto book = response.getData();


        Assertions.assertEquals("valid_bookId_1", book.getUuid());
        Assertions.assertEquals("mvc_book_test_1", book.getName());
        Assertions.assertEquals("valid_authorId_1", book.getAuthor().getUuid());
        Assertions.assertEquals("mvc_author_test_1", book.getAuthor().getName());
        Assertions.assertEquals("valid_genreId_1", book.getGenre().getUuid());
        Assertions.assertEquals("mvc_genre_test_1", book.getGenre().getName());
    }

    @Test
    public void getBookByIdWithStatusCode4000() throws Exception {
        doThrow(new EntityNotFoundException(4000, "book not found"))
                .when(bookService)
                .getBook("invalid_id");

        mvc.perform(
                        get("/invalid_id")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status.code").value("4000"))
                .andExpect(jsonPath("$.status.description").value("book not found"))
                .andExpect(jsonPath("$.data").doesNotExist());
    }

    private static Stream<Arguments> provideArgumentsForFailToCreateBook() {
        return Stream.of(
                Arguments.of("", "not blank", "not blank"),
                Arguments.of("not blank", "", "not blank"),
                Arguments.of("not blank", "not blank", ""),
                Arguments.of("", "", "not blank"),
                Arguments.of("", "not blank", ""),
                Arguments.of("not blank", "", ""),
                Arguments.of("", "", ""),
                Arguments.of(null, "not blank", "not blank"),
                Arguments.of("not blank", null, "not blank"),
                Arguments.of("not blank", "not blank", null),
                Arguments.of(null, null, "not blank"),
                Arguments.of(null, "not blank", null),
                Arguments.of("not blank", null, null),
                Arguments.of(null, null, null)
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForFailToCreateBook")
    public void postCreateBookValidationFailTest(String name, String authorId, String genreId) throws Exception {
        ObjectMapper om = new ObjectMapper();
        ObjectNode node = om.createObjectNode();
        node.put("name", name);
        node.put("authorId", authorId);
        node.put("genreId", genreId);
        String json = om.writeValueAsString(node);

        mvc.perform(
                        post("/")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status.code").value(9000))
                .andExpect(jsonPath("$.status.description").value("Mandatory missing"))
                .andExpect(jsonPath("$.data").doesNotExist());
    }

    @Test
    public void postCreateBookWithStatusCode200() throws Exception {
        Author author1 = new Author("valid_authorId_1", "mvc_author_test_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        Genre genre1 = new Genre("valid_genreId_1", "mvc_genre_test_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));

        doReturn(new BookDto("valid_bookId_1", "mvc_book_test_1", author1, genre1, Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now())))
                .when(bookService)
                .create("mvc_book_test_1", "valid_authorId_1", "valid_genreId_1");
        mvc.perform(
                post("/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"mvc_book_test_1\", \"authorId\": \"valid_authorId_1\", \"genreId\": \"valid_genreId_1\"}")
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status.code").value(200))
                .andExpect(jsonPath("$.status.description").value("OK"))
                .andExpect(jsonPath("$.data.uuid").value("valid_bookId_1"))
                .andExpect(jsonPath("$.data.name").value("mvc_book_test_1"))
                .andExpect(jsonPath("$.data.author.uuid").value("valid_authorId_1"))
                .andExpect(jsonPath("$.data.author.name").value("mvc_author_test_1"))
                .andExpect(jsonPath("$.data.genre.uuid").value("valid_genreId_1"))
                .andExpect(jsonPath("$.data.genre.name").value("mvc_genre_test_1"));
    }

    private static Stream<Arguments> provideArgumentsForCreateBookWithStatusX000() {
        return Stream.of(
                Arguments.of(2000, "author not found", "mvc_book_test_1", "invalid_authorId_1", "valid_genreId_1"),
                Arguments.of(3000, "genre not found", "mvc_book_test_1", "valid_authorId_1", "invalid_genreId_1")
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForCreateBookWithStatusX000")
    public void postCreateBookWithStatusCodeX000(int expectedCode, String expectedDescription, String name, String authorId, String genreId) throws Exception {
        doThrow(new EntityNotFoundException(expectedCode, expectedDescription))
                .when(bookService)
                .create(name, authorId, genreId);

        ObjectMapper om = new ObjectMapper();
        ObjectNode node = om.createObjectNode();
        node.put("name", name);
        node.put("authorId", authorId);
        node.put("genreId", genreId);
        String json = om.writeValueAsString(node);

        mvc.perform(
                        post("/")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status.code").value(expectedCode))
                .andExpect(jsonPath("$.status.description").value(expectedDescription))
                .andExpect(jsonPath("$.data").doesNotExist());
    }

    private static Stream<Arguments> provideArgumentsForSuccessToUpdateBook() {
        return Stream.of(
                Arguments.of("changed_mvc_book_test_1", "valid_bookId_1", "changed_valid_authorId_1", "changed_valid_genreId_1"),
                Arguments.of("changed_mvc_book_test_1", "valid_bookId_1", "changed_valid_authorId_1", ""),
                Arguments.of("changed_mvc_book_test_1", "valid_bookId_1", "changed_valid_authorId_1", null)
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForSuccessToUpdateBook")
    public void putUpdateBookWithStatusCode200(String name, String bookId, String authorId, String genreId) throws Exception {
        Author author = new Author(authorId, "changed_mvc_author_test_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        Genre genre = new Genre(genreId, "changed_mvc_genre_test_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));

        doReturn(new BookDto(bookId, name, author, genre, Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now())))
                .when(bookService)
                .update(bookId, name, authorId, genreId);

        ObjectMapper om = new ObjectMapper();
        ObjectNode node = om.createObjectNode();
        node.put("name", name);
        node.put("authorId", authorId);
        node.put("genreId", genreId);
        String json = om.writeValueAsString(node);

        mvc.perform(
                        put("/{bookId}", bookId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status.code").value(200))
                .andExpect(jsonPath("$.status.description").value("OK"))
                .andExpect(jsonPath("$.data.uuid").value(bookId))
                .andExpect(jsonPath("$.data.name").value(name))
                .andExpect(jsonPath("$.data.author.uuid").value(authorId))
                .andExpect(jsonPath("$.data.author.name").value("changed_mvc_author_test_1"))
                .andExpect(jsonPath("$.data.genre.uuid").value(genreId))
                .andExpect(jsonPath("$.data.genre.name").value("changed_mvc_genre_test_1"));
    }

    private static Stream<Arguments> provideArgumentsForFailToUpdateBook() {
        return Stream.of(
                Arguments.of("", "not blank", "not blank"),
                Arguments.of("not blank", "", "not blank"),
                Arguments.of("", "", "not blank"),
                Arguments.of("", "not blank", ""),
                Arguments.of("not blank", "", ""),
                Arguments.of("", "", ""),
                Arguments.of(null, "not blank", "not blank"),
                Arguments.of("not blank", null, "not blank"),
                Arguments.of(null, null, "not blank"),
                Arguments.of(null, "not blank", null),
                Arguments.of("not blank", null, null),
                Arguments.of(null, null, null)
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForFailToUpdateBook")
    public void putUpdateBookValidationFailTest(String name, String authorId, String genreId) throws Exception {
        ObjectMapper om = new ObjectMapper();
        ObjectNode node = om.createObjectNode();
        node.put("name", name);
        node.put("authorId", authorId);
        node.put("genreId", genreId);
        String json = om.writeValueAsString(node);

        mvc.perform(
                        put("/{bookId}", "valid_bookId_1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(String.format(json))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status.code").value(9000))
                .andExpect(jsonPath("$.status.description").value("Mandatory missing"))
                .andExpect(jsonPath("$.data").doesNotExist());
    }
    private static Stream<Arguments> provideArgumentsForUpdateBookWithStatusX000() {
        return Stream.of(
                Arguments.of(2000, "author not found", "valid_bookId_1", "mvc_book_test_1", "invalid_authorId_1", "valid_genreId_1"),
                Arguments.of(3000, "genre not found", "valid_bookId_1", "mvc_book_test_1", "valid_authorId_1", "invalid_genreId_1"),
                Arguments.of(4000, "book not found", "invalid_bookId_1", "mvc_book_test_1", "valid_authorId_1", "invalid_genreId_1")
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForUpdateBookWithStatusX000")
    public void putUpdateBookWithStatusCodeX000(int expectedCode, String expectedDescription, String bookId, String name, String authorId, String genreId) throws Exception {
        doThrow(new EntityNotFoundException(expectedCode, expectedDescription))
                .when(bookService)
                .update(bookId, name, authorId, genreId);

        ObjectMapper om = new ObjectMapper();
        ObjectNode node = om.createObjectNode();
        node.put("name", name);
        node.put("authorId", authorId);
        node.put("genreId", genreId);
        String json = om.writeValueAsString(node);

        mvc.perform(
                        put("/{bookId}", bookId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status.code").value(expectedCode))
                .andExpect(jsonPath("$.status.description").value(expectedDescription))
                .andExpect(jsonPath("$.data").doesNotExist());
    }

    private static Stream<Arguments> provideArgumentsForSuccessToDeleteBook() {
        return Stream.of(
                Arguments.of("mvc_book_test_1", "valid_bookId_1", "valid_authorId_1", "valid_genreId_1"),
                Arguments.of("mvc_book_test_1", "valid_bookId_1", "valid_authorId_1", ""),
                Arguments.of("mvc_book_test_1", "valid_bookId_1", "valid_authorId_1", null)
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForSuccessToDeleteBook")
    public void deleteDeleteBookWithStatusCode200(String name, String bookId, String authorId, String genreId) throws Exception {
        doNothing().when(bookService).delete(bookId, authorId, genreId);

        ObjectMapper om = new ObjectMapper();
        ObjectNode node = om.createObjectNode();
        node.put("name", name);
        node.put("authorId", authorId);
        node.put("genreId", genreId);
        String json = om.writeValueAsString(node);

        mvc.perform(
                        delete("/{bookId}", bookId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status.code").value(200))
                .andExpect(jsonPath("$.status.description").value("OK"))
                .andExpect(jsonPath("$.data").doesNotExist());
    }

    private static Stream<Arguments> provideArgumentsForFailToDeleteBook() {
        return Stream.of(
                Arguments.of("", "not blank", "not blank"),
                Arguments.of("not blank", "", "not blank"),
                Arguments.of("", "", "not blank"),
                Arguments.of("", "not blank", ""),
                Arguments.of("not blank", "", ""),
                Arguments.of("", "", ""),
                Arguments.of(null, "not blank", "not blank"),
                Arguments.of("not blank", null, "not blank"),
                Arguments.of(null, null, "not blank"),
                Arguments.of(null, "not blank", null),
                Arguments.of("not blank", null, null),
                Arguments.of(null, null, null)
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForFailToDeleteBook")
    public void deleteDeleteBookValidationFailTest(String name, String authorId, String genreId) throws Exception {
        ObjectMapper om = new ObjectMapper();
        ObjectNode node = om.createObjectNode();
        node.put("name", name);
        node.put("authorId", authorId);
        node.put("genreId", genreId);
        String json = om.writeValueAsString(node);

        mvc.perform(
                        delete("/{bookId}", "valid_bookId_1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(String.format(json))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status.code").value(9000))
                .andExpect(jsonPath("$.status.description").value("Mandatory missing"))
                .andExpect(jsonPath("$.data").doesNotExist());
    }
    private static Stream<Arguments> provideArgumentsForDeleteBookWithStatusX000() {
        return Stream.of(
                Arguments.of(2000, "author not found", "valid_bookId_1", "mvc_book_test_1", "invalid_authorId_1", "valid_genreId_1"),
                Arguments.of(3000, "genre not found", "valid_bookId_1", "mvc_book_test_1", "valid_authorId_1", "invalid_genreId_1"),
                Arguments.of(4000, "book not found", "invalid_bookId_1", "mvc_book_test_1", "valid_authorId_1", "invalid_genreId_1")
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForDeleteBookWithStatusX000")
    public void deleteDeleteBookWithStatusCodeX000(int expectedCode, String expectedDescription, String bookId, String name, String authorId, String genreId) throws Exception {
        doThrow(new EntityNotFoundException(expectedCode, expectedDescription))
                .when(bookService)
                .delete(bookId, authorId, genreId);

        ObjectMapper om = new ObjectMapper();
        ObjectNode node = om.createObjectNode();
        node.put("name", name);
        node.put("authorId", authorId);
        node.put("genreId", genreId);
        String json = om.writeValueAsString(node);

        mvc.perform(
                        delete("/{bookId}", bookId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status.code").value(expectedCode))
                .andExpect(jsonPath("$.status.description").value(expectedDescription))
                .andExpect(jsonPath("$.data").doesNotExist());
    }

    private BooksByAuthorDto mockGetBooksByAuthor() {
        Author author1 = new Author("valid_authorId_1", "mvc_author_test_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        Genre genre1 = new Genre("valid_genreId_1", "mvc_genre_test_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));

        BookDto book1 = new BookDto("valid_bookId_1", "mvc_book_test_1", author1, genre1, Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        BookDto book2 = new BookDto("valid_bookId_2", "mvc_book_test_2", author1, genre1, Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));

        List<BookDto> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);

        BooksByAuthorDto booksByAuthorDto = new BooksByAuthorDto(author1.getUuid(), author1.getName(), books);

        return booksByAuthorDto;
    }

    @Test
    public void getBooksByAuthorWithStatusCode200() throws Exception {
        doReturn(mockGetBooksByAuthor())
                .when(bookService)
                .getBooksByAuthor("valid_authorId_1");

        MvcResult mvcResult = mvc.perform(
                        get("/valid_authorId_1/author")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        ResponseObject<BooksByAuthorDto> response = mapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<ResponseObject<BooksByAuthorDto>>() {});
        BooksByAuthorDto booksByAuthorDto = response.getData();

        Assertions.assertEquals("valid_authorId_1", booksByAuthorDto.getUuid());
        Assertions.assertEquals("mvc_author_test_1", booksByAuthorDto.getName());
        Assertions.assertEquals(2, booksByAuthorDto.getBookList().size());

        Assertions.assertEquals("valid_bookId_1", booksByAuthorDto.getBookList().get(0).getUuid());
        Assertions.assertEquals("mvc_book_test_1", booksByAuthorDto.getBookList().get(0).getName());
        Assertions.assertEquals("valid_authorId_1", booksByAuthorDto.getBookList().get(0).getAuthor().getUuid());
        Assertions.assertEquals("valid_bookId_2", booksByAuthorDto.getBookList().get(1).getUuid());
        Assertions.assertEquals("mvc_book_test_2", booksByAuthorDto.getBookList().get(1).getName());
        Assertions.assertEquals("valid_authorId_1", booksByAuthorDto.getBookList().get(1).getAuthor().getUuid());
    }

    @Test
    public void getBooksByAuthorWithStatusCode4000() throws Exception {
        doThrow(new EntityNotFoundException(4000, "book not found"))
                .when(bookService)
                .getBooksByAuthor("invalid_authorId_1");

        mvc.perform(
                        get("/invalid_authorId_1/author")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status.code").value("4000"))
                .andExpect(jsonPath("$.status.description").value("book not found"))
                .andExpect(jsonPath("$.data").doesNotExist());
    }

    private BooksByGenreDto mockGetBooksByGenre() {
        Author author1 = new Author("valid_authorId_1", "mvc_author_test_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        Genre genre1 = new Genre("valid_genreId_1", "mvc_genre_test_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));

        BookDto book1 = new BookDto("valid_bookId_1", "mvc_book_test_1", author1, genre1, Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        BookDto book2 = new BookDto("valid_bookId_2", "mvc_book_test_2", author1, genre1, Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));

        List<BookDto> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);

        BooksByGenreDto booksByGenreDto = new BooksByGenreDto(genre1.getUuid(), genre1.getName(), books);

        return booksByGenreDto;
    }

    @Test
    public void getBooksByGenreWithStatusCode200() throws Exception {
        doReturn(mockGetBooksByGenre())
                .when(bookService)
                .getBooksByGenre("valid_genreId_1");

        MvcResult mvcResult = mvc.perform(
                        get("/valid_genreId_1/genre")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        ResponseObject<BooksByGenreDto> response = mapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<ResponseObject<BooksByGenreDto>>() {});
        BooksByGenreDto booksByGenreDto  = response.getData();

        Assertions.assertEquals("valid_genreId_1", booksByGenreDto.getUuid());
        Assertions.assertEquals("mvc_genre_test_1", booksByGenreDto.getName());
        Assertions.assertEquals(2, booksByGenreDto.getBookList().size());

        Assertions.assertEquals("valid_bookId_1", booksByGenreDto.getBookList().get(0).getUuid());
        Assertions.assertEquals("mvc_book_test_1", booksByGenreDto.getBookList().get(0).getName());
        Assertions.assertEquals("valid_genreId_1", booksByGenreDto.getBookList().get(0).getGenre().getUuid());
        Assertions.assertEquals("valid_bookId_2", booksByGenreDto.getBookList().get(1).getUuid());
        Assertions.assertEquals("mvc_book_test_2", booksByGenreDto.getBookList().get(1).getName());
        Assertions.assertEquals("valid_genreId_1", booksByGenreDto.getBookList().get(1).getGenre().getUuid());
    }

    @Test
    public void getBooksByGenreWithStatusCode4000() throws Exception {
        doThrow(new EntityNotFoundException(4000, "book not found"))
                .when(bookService)
                .getBooksByGenre("invalid_genreId_1");

        mvc.perform(
                        get("/invalid_genreId_1/genre")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status.code").value("4000"))
                .andExpect(jsonPath("$.status.description").value("book not found"))
                .andExpect(jsonPath("$.data").doesNotExist());
    }

}

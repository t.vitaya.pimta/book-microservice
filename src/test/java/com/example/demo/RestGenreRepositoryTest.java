package com.example.demo;

import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.models.Author;
import com.example.demo.models.Genre;
import com.example.demo.repository.RestGenreRepository;
import com.example.demo.utils.Response;
import com.example.demo.utils.ResponseObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
public class RestGenreRepositoryTest {
    @InjectMocks
    RestGenreRepository genreRepository;

    @Mock
    RestTemplate restTemplate;

    @Test
    public void getById() throws EntityNotFoundException {
        ReflectionTestUtils.setField(genreRepository, "genreBaseUrl", "/");
        Genre genre = new Genre("valid_genreId_1", "genre_test_1", Timestamp.valueOf(LocalDateTime.now()), Timestamp.valueOf(LocalDateTime.now()));
        String url = "/valid_genreId_1";

        doReturn(new Response<Genre>(genre)).when(restTemplate).exchange(
                eq(url),
                eq(HttpMethod.GET),
                Matchers.<HttpEntity<ResponseObject<Genre>>>any(),
                Matchers.<ParameterizedTypeReference<ResponseObject<Genre>>>any()
        );

        Genre getGenre = genreRepository.getById("valid_genreId_1");

        Assertions.assertEquals("valid_genreId_1", getGenre.getUuid());
        Assertions.assertEquals("genre_test_1", getGenre.getName());
    }

    @Test
    public void getByIdFail() {
        ReflectionTestUtils.setField(genreRepository, "genreBaseUrl", "/");
        String url = "/invalid_genreId_1";

        doReturn(new Response<>(3000, "genre not found")).when(restTemplate).exchange(
                eq(url),
                eq(HttpMethod.GET),
                Matchers.<HttpEntity<ResponseObject<Author>>>any(),
                Matchers.<ParameterizedTypeReference<ResponseObject<Author>>>any()
        );

        Assertions.assertThrows(EntityNotFoundException.class, () -> genreRepository.getById("invalid_genreId_1"));
    }
}
